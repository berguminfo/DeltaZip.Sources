# DeltaZip

Simple library to create and apply delta compressed zip files.

## Build

```bash
    msbuild src\DeltaZip.csproj
```

