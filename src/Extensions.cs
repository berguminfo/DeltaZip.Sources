// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;
using System.IO;
using Ionic.Zip;

namespace DeltaZip {

    internal static partial class Extensions {

        internal static void CopyAttributes (this ZipEntry source, ZipEntry destination) {
            destination.Attributes = source.Attributes;
        }

        internal static void CopyEntryTimes (this ZipEntry source, ZipEntry destination) {
            destination.SetEntryTimes (source.CreationTime, source.AccessedTime, source.ModifiedTime);
        }

        internal static void CopyMetadata (this ZipEntry source, ZipEntry destination) {
            destination.CompressionMethod = CompressionMethod.BZip2;
            destination.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
            destination.IsText = source.IsText;
            source.CopyAttributes (destination);
            source.CopyEntryTimes (destination);
        }

        internal static byte[] GetByteContents (this ZipEntry entry) {
            var buffer = new MemoryStream ((int)entry.UncompressedSize);
            entry.Extract (buffer);

            var data = buffer.ToArray ();
            if (data.Length != entry.UncompressedSize) {
                throw new InvalidOperationException ($"Expected {entry.UncompressedSize} bytes, got {data.Length}");
            }

            return data;
        }
    }
}