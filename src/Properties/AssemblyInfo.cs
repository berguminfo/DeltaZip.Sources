// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle ("DeltaZip")]
[assembly: AssemblyDescription ("DeltaZip.dll")]
[assembly: AssemblyCulture ("")]

// Assemblies should explicitly state their CLS compliance using the CLSCompliant attribute
[assembly: CLSCompliant (true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible (false)]
