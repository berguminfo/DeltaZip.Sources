// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;

namespace DeltaZip {

    public sealed partial class Patcher {

        public bool WithDeletePatch { get; set; } = true;

        public bool WithAttributesPatch { get; set; } = true;

        public bool WithTimesPatch { get; set; } = false;

        public bool WithContentsPatch { get; set; } = true;

        public bool WithNewPatch { get; set; } = true;

        public EventHandler<ProgressMessage> PatchProgress { get; set; }

        public Patcher () {
        }

        public Patcher (EventHandler<ProgressMessage> patchProgress) {
            PatchProgress = patchProgress;
        }

        void Progress (string format, params object [] args) {
            Progress (PatchFlag.None, format, args);
        }

        void Progress (PatchFlag flag, string format, params object[] args) {
            PatchProgress?.Invoke (this, new ProgressMessage (
                flag, string.Format (format, args)));
        }
    }
}