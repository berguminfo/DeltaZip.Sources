// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;
using System.Text;

namespace DeltaZip {

    [Flags]
    // ABCDEFG
    public enum PatchFlag {
        None   = 0x00,
        Delete = 0x01,
        //Rename = 0x02,
        Attr   = 0x04,
        Time   = 0x08,
        Patch  = 0x10,
        New    = 0x20
    }

    public partial class PatchEntry {

        const int CodeOffset = 0x3B;

        public PatchFlag Flags { get; set; } = PatchFlag.None;

        public string Extra { get; set; }

        public static PatchEntry Deserialize (string s) {
            var result = new PatchEntry ();

            if (!string.IsNullOrEmpty (s)) {
                if (s.Length >= 1) {
                    result.Flags = (PatchFlag)(s [0] - CodeOffset);
                }
                if (s.Length >= 2) {
                    result.Extra = s.Substring (1);
                }
            }

            return result;
        }

        public string SerializeToString () {
            var buffer = new StringBuilder ();
            buffer.Append ((char)(Flags + CodeOffset));
            if (Extra != null) {
                buffer.Append (Extra);
            }

            return buffer.ToString ();
        }
    }
}