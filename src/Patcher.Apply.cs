// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;
using System.IO;
using System.Linq;
using Ionic.Zip;

namespace DeltaZip {

    partial class Patcher {

        public void Apply (string oldFileName, string deltaFileName, string newFileName) {
            if (oldFileName == null) {
                throw new ArgumentNullException (nameof (oldFileName));
            }
            if (deltaFileName == null) {
                throw new ArgumentNullException (nameof (deltaFileName));
            }
            if (newFileName == null) {
                throw new ArgumentNullException (nameof (newFileName));
            }

            // copy oldzip to newzip in order to avoid internal decompress -> compress
            File.Copy (oldFileName, newFileName, true);

            using (var oldZip = ZipFile.Read (oldFileName)) {
                using (var deltaZip = ZipFile.Read (deltaFileName)) {
                    using (var newZip = ZipFile.Read (newFileName)) {
                        Apply (oldZip, deltaZip, newZip);
                        newZip.Save ();
                        Progress ($"saved `{newFileName}'");
                    }
                }
            }
        }

        void Apply (ZipFile oldZip, ZipFile deltaZip, ZipFile newZip) {

            // create hashtable of delta entries
            var newFiles = newZip.Entries.ToDictionary (x => x.FileName, x => x);

            ZipEntry newEntry;
            foreach (var deltaEntry in deltaZip.Entries) {
                if (!newFiles.TryGetValue (deltaEntry.FileName, out newEntry)) {

                    // old zip don't contain the delta entry, hence copy the file
                    if (WithNewPatch) {
                        Progress (PatchFlag.New, "{0} ({1})", deltaEntry.FileName, deltaEntry.UncompressedSize);
                        var entry = newZip.AddEntry (deltaEntry.FileName, deltaEntry.GetByteContents ());
                        deltaEntry.CopyMetadata (entry);
                    }
                } else {
                    var patchEntry = PatchEntry.Deserialize (deltaEntry.Comment);

                    // content has changed
                    if (WithContentsPatch && patchEntry.Flags.HasFlag (PatchFlag.Patch)) {
                        Progress (PatchFlag.Patch, deltaEntry.FileName);
                        var byteContents = ApplyPatch (newEntry, deltaEntry);
                        newZip.UpdateEntry (deltaEntry.FileName, byteContents);
                    } else

                    // file has been deleted
                    if (WithDeletePatch && patchEntry.Flags.HasFlag (PatchFlag.Delete)) {
                        Progress (PatchFlag.Delete, deltaEntry.FileName);
                        newZip.RemoveEntry (deltaEntry);
                    } else

                    // attributes has changed
                    if (WithAttributesPatch && patchEntry.Flags.HasFlag (PatchFlag.Attr)) {
                        Progress (PatchFlag.Attr, "{0} (0x{1:x} -> 0x{2:x})",
                            deltaEntry.FileName, newEntry.Attributes, deltaEntry.Attributes);
                        newEntry.CopyAttributes (deltaEntry);
                    } else

                    // entry time has changed
                    if (WithTimesPatch && patchEntry.Flags.HasFlag (PatchFlag.Time)) {
                        Progress (PatchFlag.Time, "{0} ({1} -> {2})",
                            deltaEntry.FileName, newEntry.LastModified, deltaEntry.LastModified);
                        deltaEntry.CopyEntryTimes (newEntry);
                    } else {

                        Progress ($">>> Unknown flag: {patchEntry.Flags}");
                    }
                }
            }
        }

        byte[] ApplyPatch (ZipEntry oldEntry, ZipEntry deltaEntry) {
            var oldStream = new MemoryStream (oldEntry.GetByteContents ());
            var deltaData = deltaEntry.GetByteContents ();
            var newStream = new MemoryStream ();

            try {
                BsDiff.BinaryPatchUtility.Apply (oldStream, () => new MemoryStream (deltaData), newStream);
            } catch (Exception e) {
                Progress ($">>> {e.Message}");
#if DEBUG
                throw;
#endif
            }

            return newStream.ToArray ();
        }
    }
}