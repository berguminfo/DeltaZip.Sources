// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

namespace DeltaZip {

    public sealed class ProgressMessage {

        public PatchFlag Flag { get; }

        public string Message { get; }

        public ProgressMessage (PatchFlag flag, string message) {
            Flag = flag;
            Message = message;
        }
    }
}