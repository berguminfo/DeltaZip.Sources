// DeltaZip
// © 2017 BERGUM INFO. All rights reserved.
// See LICENSE file in the project root for full license information.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zip;

namespace DeltaZip {

    partial class Patcher {

        public void Create (string oldFileName, string newFileName, string deltaFileName) {
            if (oldFileName == null) {
                throw new ArgumentNullException (nameof (oldFileName));
            }
            if (newFileName == null) {
                throw new ArgumentNullException (nameof (newFileName));
            }
            if (deltaFileName == null) {
                throw new ArgumentNullException (nameof (deltaFileName));
            }

            using (var deltaStream = File.Create (deltaFileName)) {
                Create (oldFileName, newFileName, deltaStream);
                Progress ($"saved `{deltaFileName}'");
            }
        }

        public void Create (string oldFileName, string newFileName, Stream deltaStream) {
            if (oldFileName == null) {
                throw new ArgumentNullException (nameof (oldFileName));
            }
            if (newFileName == null) {
                throw new ArgumentNullException (nameof (newFileName));
            }
            if (deltaStream == null) {
                throw new ArgumentNullException (nameof (deltaStream));
            }

            using (var oldZip = ZipFile.Read (oldFileName)) {
                using (var newZip = ZipFile.Read (newFileName)) {
                    using (var deltaZip = new ZipFile ()) {
                        Create (oldZip, newZip, deltaZip);
                        deltaZip.Save (deltaStream);
                    }
                }
            }
        }

        void Create (ZipFile oldZip, ZipFile newZip, ZipFile deltaZip) {

            // create hashtable of new entries
            var newFiles = newZip.Entries.ToDictionary (x => x.FileName, x => x);

            // the list of functions to create the patch entry
            var patchList = new List<Action<ZipEntry>> ();
            var patchEntry = new PatchEntry ();
            byte [] byteContent = null;

            foreach (var oldEntry in oldZip.Entries) {
                ZipEntry newEntry;
                if (!newFiles.TryGetValue (oldEntry.FileName, out newEntry)) {

                    // newzip don't contain old entry, so old entry has been deleted or renamed
                    // TODO: check for possible relocated entry
                    if (WithDeletePatch) {
                        Progress (PatchFlag.Delete, oldEntry.FileName);
                        patchEntry.Flags |= PatchFlag.Delete;
                    }
                } else {

                    // attributes has changed
                    if (WithAttributesPatch && oldEntry.Attributes != newEntry.Attributes) {
                        Progress (PatchFlag.Attr, "{0} (0x{1:x} -> 0x{2:x})", 
                            oldEntry.FileName, oldEntry.Attributes, newEntry.Attributes);
                        patchEntry.Flags |= PatchFlag.Attr;
                        patchList.Add (entry => newEntry.CopyAttributes (entry));
                    }

                    // entry time has changed
                    if (WithTimesPatch && oldEntry.LastModified != newEntry.LastModified) {
                        Progress (PatchFlag.Time, "{0} ({1} -> {2})", 
                            oldEntry.FileName, oldEntry.LastModified, newEntry.LastModified);
                        patchEntry.Flags |= PatchFlag.Time;
                        patchList.Add (entry => newEntry.CopyEntryTimes (entry));
                    }

                    // content has changed
                    if (WithContentsPatch && oldEntry.Crc != newEntry.Crc) {
                        Progress (PatchFlag.Patch, oldEntry.FileName);
                        byteContent = CreatePatch (oldEntry, newEntry);
                        patchEntry.Flags |= PatchFlag.Patch;
                    }
                }

                // apply all found patches
                if (patchEntry.Flags > 0) {
                    var deltaEntry = deltaZip.AddEntry (oldEntry.FileName, byteContent ?? new byte [0]);
                    deltaEntry.CompressionMethod = CompressionMethod.None;
                    deltaEntry.IsText = false;
                    deltaEntry.Comment = patchEntry.SerializeToString ();

                    foreach (var operation in patchList) {
                        operation (deltaEntry);
                    }

                    // clear current patch
                    patchEntry.Flags = PatchFlag.None;
                    byteContent = null;
                    patchList.Clear ();
                }

                // remove any found entry
                if (newEntry != null) {
                    newFiles.Remove (newEntry.FileName);
                }
            }

            // copy new files
            if (WithNewPatch) {
                foreach (var newEntry in newFiles.Values) {
                    Progress (PatchFlag.New, "{0} ({1})", newEntry.FileName, newEntry.UncompressedSize);
                    var deltaEntry = deltaZip.AddEntry (newEntry.FileName, newEntry.GetByteContents ());
                    newEntry.CopyMetadata (deltaEntry);
                }
            }
        }

        byte[] CreatePatch (ZipEntry oldEntry, ZipEntry newEntry) {
            var oldData = oldEntry.GetByteContents ();
            var newData = newEntry.GetByteContents ();

            var deltaBuffer = new MemoryStream ();
            try {
                BsDiff.BinaryPatchUtility.Create (oldData, newData, deltaBuffer);
            } catch (Exception e) {
                Progress ($">>> {e.Message}");
#if DEBUG
                throw;
#endif
            }

            return deltaBuffer.ToArray ();
        }
    }
}